# Wordpress with Docker

A simple way to get a Wordpress install up and running, using [Docker](https://www.docker.com/)

## Prerequisites

Docker installed and working (should include docker-compose)

## Usage

Bring up the containers using `docker-compose up -d`, this will start everything in 'daemon' mode.

The wordpress container will clone the most recent version of Wordpress and install it to `wordpress/`. You can then add your own plugins etc there.

Database will be persisted into `data/db/` across docker-compose restarts etc.

The wordpress installation will be available at `http://localhost:3000` - you can change this port in `docker-compose.yml` if you have other things running on port 3000.

To manage the database, you can connect to `localhost:33060` with user: `root`, pass: `root`